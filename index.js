// #Task 1

function arg(a){
    return a;
}

function div(b){
    if (b%(3*5)===0){
        console.log("3 va 5 ga bo'linadi")
    } else console.log("3 va 5 ga bo'linmaydi")
}

div(arg(45));

// #Task 2

function odd(b){
    if(b%2===0){
        console.log('Juft')
    } else console.log('toq')
}

odd(arg(33));

// #Task 3
let arr=[1,10,23,43,2,3,5,16,4];

function sort(arr){

    let sort=[],c=0;
    for(let i=0;i<arr.length;i++){
        for(let j=i;j<arr.length;j++){
            if(arr[i]>arr[j]){
                c=arr[i];
                arr[i]=arr[j];
                arr[j]=c;
            } 
        }
    }
    console.log(arr);
}

sort(arr);

// #Task 4

// let obj = [
//     {test: ['a','b','c','d']},
//     {test: ['a','b','c']},
//     {test: ['a','d']},
//     {test: ['a','b','k','e','e']}
// ]

// console.log(obj);

// const _ = require('lodash');

// const obj1 = { burger: '🍔', pizza: '🍕' };
// const obj2 = { pizza: '🍕', burger: '🍔' };

// if (_.isEqual(obj1, obj2)) {
//     console.log('Objects are equal!');
// } else {
//     console.log('Objects are not equal.');
// }

// // Objects are equal!